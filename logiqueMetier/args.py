import argparse, sys
 
def parseArg():
    """
    Parse argument, if not sufficient: default parameter
    """
    parser = argparse.ArgumentParser(description='Create simulation of wildfires')
    parser.add_argument('-rows', help = 'rows of the canvas', type=int, default=10)
    parser.add_argument('-cols', help = 'cols of the canvas', type=int, default=10)
    parser.add_argument('-cell_size', help = 'cell_size of the canvas rectangle', type=int, default=25)
    parser.add_argument('-afforestation', help= 'percentage of afforestation', type=float, default=.6)
    parser.add_argument('-anim', help= 'percentage of afforestation', type=float, default=.5)
    return parser.parse_args()

def getParametre():
    """ 
    Get list with param necessary for script
    """
    args = parseArg()
    parameter =[]
    parameter.append(args.rows)
    parameter.append(args.cols)
    parameter.append(args.cell_size)
    parameter.append(args.afforestation)
    parameter.append(args.anim)
    parameter.append(False)
    return parameter