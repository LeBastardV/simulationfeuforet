import random
from copy import deepcopy

def initializeMap(parameter):
    '''
    Initialize a list of list corespond to the map of the simulation
    parameter : [row; cols; cell_size; afforestation; animation; pause]
    '''
    forest = list()
    for index_column in range(0, parameter[1]):
        column = list()
        for index_row in range(0, parameter[0]):
            if random.randint(1, 10) <= parameter[3] * 10: column.append("green") #green for forest
            else: column.append("lightgrey") #lightgrey for nothing
        forest.append(column)
    return forest

def regenMap(parameter, forest, initialForest):
    '''
    Regeneration of the map (and change the intialMap, for reseting)
    parameter : [row; cols; cell_size; afforestation; animation, pause]
    '''
    newForest = initializeMap(parameter)
    forest[:] = deepcopy(newForest)
    initialForest[:] = deepcopy(newForest)
    return forest

def isFireOrCinder(forestMap):
    '''
    Return if there is one cinder or one fire on the map
    '''
    for column in forestMap:
        for state in column:
            if (state == "red" or state == "grey"):
                return True
    return False

def removeCinder(forestMap):
    '''
    Return the map without cinder
    '''
    for abcsissa in range(0, len(forestMap)):
        for ordinate in range(0, len(forestMap[1])):
            if(forestMap[abcsissa][ordinate]=="grey"): 
                forestMap[abcsissa][ordinate] = 'lightgrey'
    return forestMap

def fireToCinder(forestMap):
    '''
    Pass fire to cinder and return the map
    '''
    for abcsissa in range(0, len(forestMap)):
        for ordinate in range(0, len(forestMap[0])):
            if(forestMap[abcsissa][ordinate]=="red"): 
                forestMap[abcsissa][ordinate] = 'grey'
    return forestMap

def firstRuleToInflameForest(forestMap):
    '''
    Execute the first rule to infire the map, and return the map
    '''
    for abcsissa in range(0, len(forestMap)):
        for ordinate in range(0, len(forestMap[0])):
            if (forestMap[abcsissa][ordinate]=="grey"): #on test si la case est grise = t-1 en feu
                if abcsissa - 1 >= 0 and forestMap[abcsissa - 1][ordinate] == "green" :
                    forestMap[abcsissa - 1][ordinate] = "red"
                if abcsissa + 1 < len(forestMap) and forestMap[abcsissa + 1][ordinate] == "green" : 
                    forestMap[abcsissa + 1][ordinate] = "red"
                if ordinate - 1 >= 0 and forestMap[abcsissa][ordinate - 1] == "green" :
                    forestMap[abcsissa][ordinate - 1] = "red"
                if ordinate + 1 < len(forestMap[1]) and forestMap[abcsissa][ordinate + 1] == "green" :
                    forestMap[abcsissa][ordinate + 1] = "red"
    return forestMap

def secondRuleToInflameForest(forestMap):
    '''
    Execute the second rule to infire the map, and return the map
    '''
    for abcsissa in range(len(forestMap)):
        for ordinate in range(len(forestMap[1])):
            if forestMap[abcsissa][ordinate] == "green" :
                inflameNeighbor = 0
                if abcsissa - 1 >= 0 and forestMap[abcsissa - 1][ordinate] == "grey" :
                        inflameNeighbor += 1
                if abcsissa + 1 < len(forestMap) and forestMap[abcsissa + 1][ordinate] == "grey" : 
                        inflameNeighbor += 1
                if ordinate - 1 >= 0 and forestMap[abcsissa][ordinate - 1] == "grey" :
                         inflameNeighbor += 1
                if ordinate + 1 < len(forestMap[1]) and forestMap[abcsissa][ordinate + 1] == "grey" :
                        inflameNeighbor += 1
                if random.random() <= 1-(1/(inflameNeighbor+1)):
                    forestMap[abcsissa][ordinate] = "red"
    return forestMap
