import tkinter as tk
from tkinter import ttk
from logiqueMetier.logiqueMetier import *
from logiqueMetier.args import *
from interface.callback import *
import time
from interface.window import *
import platform
import sys
## python3 app.py -rows=20 -cols=35 -cell_size=25 -afforestation=.6 -anim=2 

def runSimu(canvas, forestMap, parameter, regle):
    '''
    to run the simulation
    '''
    while(isFireOrCinder(forestMap)):
        if(parameter[5]): continue
        else:
            forestMap = removeCinder(forestMap)
            forestMap = fireToCinder(forestMap)
            if (regle == "Regle 1"):
                forestMap = firstRuleToInflameForest(forestMap)
            else:
                forestMap = secondRuleToInflameForest(forestMap)
            makeCanvas(canvas, forestMap, parameter[2])
            time.sleep(parameter[4])

if __name__ == '__main__' :

    parameter = getParametre()
    
    forestMap = initializeMap(parameter)
    initialForest = deepcopy(forestMap)

    master = createWindow()
    canvas = createCanvas(parameter, master)
    makeCanvas(canvas, forestMap, parameter[2])

    #Binding
    canvas.bind('<Button-1>', lambda event : inflame(event, canvas, parameter[2], forestMap))
    if(platform.system() == 'Darwin'): #For MacOS Systems
        canvas.bind('<Button-2>', lambda event : removeFire(event, canvas, parameter[2], forestMap))
    else: #For other Systems (Linux and Windows)
        canvas.bind('<Button-3>', lambda event : removeFire(event, canvas, parameter[2], forestMap))


    #Create Buttons
    btnSimu = Button(master, text="Lancer", state='normal',
        command= lambda : callbackLancer(canvas, forestMap, parameter, combo.get(), stateBtn))
    btnSimu.grid(row = 1, column = 0)

    btnPause = Button(master, text="Pause", state='disabled',
        command= lambda : callbackPause(parameter, stateBtn))
    btnPause.grid(row = 1, column = 1)

    btnReset = Button(master, text="Reset", state='normal',
        command= lambda : callback_reset(canvas, initialForest, parameter[2], forestMap, stateBtn))
    btnReset.grid(row = 1, column = 2)

    btnRegenMap = Button(master, text="Regeneration", state='normal',
        command= lambda : callback_regeneration(parameter, canvas, forestMap, initialForest, stateBtn))
    btnRegenMap.grid(row = 1, column = 3)

    #Create Combobox
    combo = ttk.Combobox(master, values=["Regle 1", "Regle 2"], state='readonly', 
        width=7, justify="center")
    combo.current(0)
    combo.grid(row = 1, column = 4)

    stateBtn = [btnSimu, btnPause, btnReset, btnRegenMap, combo]

    master.mainloop()
