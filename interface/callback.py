from tkinter import *
from logiqueMetier.logiqueMetier import *
from interface.window import *
from app import *
import threading

def callback_regeneration(parameter, canvas, forestMap, initialForest,stateBtn):
    '''
    Callback to regen a map
    parameter : [row; cols; cell_size; afforestation; animation, pause]
    '''
    stateBtn[0].config(state='normal')
    stateBtn[1].config(state='disabled')
    stateBtn[2].config(state='normal')
    stateBtn[3].config(state='normal')
    stateBtn[4].config(state='readonly')
    regenMap(parameter, forestMap, initialForest)
    refreshCanvas(canvas, forestMap, parameter[2])

def callback_reset(canvas, initialForest, cellSize, forestMap, stateBtn):
    '''
    Callback to reset map
    '''
    stateBtn[0].config(state='normal')
    stateBtn[1].config(state='disabled')
    stateBtn[2].config(state='normal')
    stateBtn[3].config(state='normal')
    stateBtn[4].config(state='readonly')
    resetCanvas(canvas, initialForest, cellSize, forestMap)

def callbackLancer(canvas, forestMap, parameter, regle, stateBtn):
    '''
    Callback to play
    '''
    stateBtn[0].config(state='disabled')
    stateBtn[1].config(state='normal')
    stateBtn[2].config(state='normal')
    stateBtn[3].config(state='normal')
    stateBtn[4].config(state='disabled')
    parameter[5]=False
    th = threading.Thread(target=runSimu, args=(canvas, forestMap, parameter, regle))
    th.start()

def inflame(event, canvas, cellSize, forest):
    '''
    inflame a case
    '''
    x=int(event.x/cellSize)
    y=int(event.y/cellSize)
    if(forest[x][y] == "green"):
        forest[x][y] = "red"
        refreshCanvas(canvas,forest, cellSize)

def removeFire(event, canvas, cellSize, forest):
    '''
    remove fire of case
    '''
    x=int(event.x/cellSize)
    y=int(event.y/cellSize)
    if(forest[x][y]=="red"):
        forest[x][y] = "green"
        refreshCanvas(canvas,forest, cellSize)

def callbackPause(parameter, stateBtn):
    '''
    Callback to break simulation
    '''
    if(parameter[5]): 
        parameter[5] = False # retire la pause
        stateBtn[0].config(state='disabled')
        stateBtn[1].config(state='normal')
        stateBtn[2].config(state='normal')
        stateBtn[3].config(state='normal')
        stateBtn[4].config(state='disabled')
    else: 
        parameter[5] = True
        stateBtn[0].config(state='disabled')
        stateBtn[1].config(state='normal')
        stateBtn[2].config(state='normal')
        stateBtn[3].config(state='normal')
        stateBtn[4].config(state='readonly')
        
            