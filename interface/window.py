from tkinter import *
from copy import deepcopy

def makeCanvas(canvas, forest, cellSize):
    '''
    Create the rectangle in the canvas with the forest map and cellSize
    canvas : canvas of the script
    forest : list of list corespond to the map 
    cellSize : size of cell
    '''
    abcsissa = 0
    for column in forest:
        ordinate = 0
        for state in column:
            canvas.create_rectangle(abcsissa, ordinate, abcsissa + cellSize, ordinate + cellSize, fill=state)
            ordinate += cellSize
        abcsissa +=cellSize

def refreshCanvas(canvas, forest, cellSize):
    '''
    Delete actual canvas and refresh this
    '''
    canvas.delete('all')
    makeCanvas(canvas, forest, cellSize)

def resetCanvas(canvas, initialForest, cellSize, forestMap):
    '''
    Replace the forest map (eventually modified with simulation) by the initial forest map,
    and refresh canvas
    '''
    forestMap[:]=deepcopy(initialForest)
    refreshCanvas(canvas, forestMap, cellSize)

def createCanvas(parameter, master):
    '''
    Create a canvas with parameter 
    parameter : [row; cols; cell_size; afforestation; animation, pause]
    '''
    if parameter[1]*parameter[2]>master.winfo_screenwidth() or parameter[0]*parameter[2] > master.winfo_screenheight():
        print("La fenetre ne rentre pas dans ton écran, change les dimensions stp")
        exit(1)
    else:
        canvas = Canvas(master, width=parameter[1]*parameter[2], height=parameter[0]*parameter[2])
        canvas.grid(row = 0, column = 0, columnspan=5, padx=10, pady=10)
    return canvas

def createWindow():
    '''
    Create a window interface
    '''
    master = Tk()
    master.resizable(width = False, height = False)
    master.title("Simulation of forest fire spread")
    return master