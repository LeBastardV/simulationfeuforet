import unittest
from logiqueMetier.logiqueMetier import *
from copy import deepcopy
from interface.callback import *
from logiqueMetier.args import *

class TestMap(unittest.TestCase):
    
    def setUp(self):
        self.parameter = [5, 5, 25, .6, .5, False]
        self.forestMap = initializeMap(self.parameter)
        self.initialForestMap = deepcopy(self.forestMap)
        self.mapWithFireAndCinder = [['green', 'lightgrey', 'grey'],['red','grey','green'],['red','grey','green']]

    def test_initializeMap(self):
        self.assertIsInstance(self.forestMap, list)
        self.assertEqual(len(self.forestMap), 5)

    def test_regenMap(self):
        firstMap = deepcopy(self.forestMap)
        self.assertNotEqual(firstMap, regenMap(self.parameter, self.forestMap, self.initialForestMap))

    def test_isFireOrCinder(self):
        self.assertFalse(isFireOrCinder(self.forestMap))

    def test_removeCinder(self):
        mapTest = removeCinder(self.mapWithFireAndCinder)
        counterCinder = 0
        for abcsissa in range(0, len(mapTest)):
            for ordinate in range(0, len(mapTest[1])):
                if(mapTest[abcsissa][ordinate]=="grey"): 
                    counterCinder += 1

        self.assertEqual(counterCinder, 0)

    def test_fireToCinder(self):
        counterFire = 0
        counterCinder = 0
        for abcsissa in range(0, len(self.mapWithFireAndCinder)):
            for ordinate in range(0, len(self.mapWithFireAndCinder[1])):
                if(self.mapWithFireAndCinder[abcsissa][ordinate]=="red"): 
                    counterFire += 1
                if(self.mapWithFireAndCinder[abcsissa][ordinate]=="grey"): 
                    counterCinder += 1
        testMap = fireToCinder(self.mapWithFireAndCinder)
        counterFireAfter = 0
        counterCinderAfter = 0
        for abcsissa in range(0, len(testMap)):
            for ordinate in range(0, len(testMap[1])):
                if(testMap[abcsissa][ordinate]=="red"): 
                    counterFireAfter += 1
                if(testMap[abcsissa][ordinate]=="grey"): 
                    counterCinderAfter += 1
                    
        self.assertEqual(counterFireAfter, 0)
        self.assertEqual(counterCinderAfter, counterFire +  counterCinder)